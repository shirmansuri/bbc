// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseconfig:{
    apiKey: "AIzaSyA6zRJz3Vu9aj7loVA_NfXk7w728ElJS48",
    authDomain: "bbc1-4ac8c.firebaseapp.com",
    databaseURL: "https://bbc1-4ac8c.firebaseio.com",
    projectId: "bbc1-4ac8c",
    storageBucket: "bbc1-4ac8c.appspot.com",
    messagingSenderId: "652058014067",
    appId: "1:652058014067:web:f375568e1d36357cc11506",
    measurementId: "G-ZQ6L1VWCXD"
  }
  };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
