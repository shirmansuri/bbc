import { Component, OnInit } from '@angular/core';
import { PostsService } from './../posts.service';    


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  panelOpenState = false;
  posts$
  users$
  constructor(private postservice:PostsService) { }

  ngOnInit() {

    this.posts$ = this.postservice.getPost();
    this.users$ = this.postservice.getUser();
    
  }
 
  
}
