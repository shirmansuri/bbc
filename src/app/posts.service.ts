import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from './interfaces/post';
import { User } from './interfaces/user';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private API = "https://jsonplaceholder.typicode.com/posts"
  private userAPI = "https://jsonplaceholder.typicode.com/users"

  
// constructor(private http: HttpClient) { }
constructor(
  private db:AngularFirestore,
  private http:HttpClient
  ) {}
  getPost(): Observable<Post[]>
 {
    return this.db.collection<Post>("posts").valueChanges();
    // return this.http.get<Post>(`${this.API}`);
  }
  getUser(): Observable<User>
 {
  
    return this.http.get<User>(`${this.userAPI}`);
  
  }
}