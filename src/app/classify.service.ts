import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
  private url = "https://9qz18oddh0.execute-api.us-east-1.amazonaws.com/beta"; // Shir
  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}
  userCollection: AngularFirestoreCollection= this.db.collection('Users');
  public doc:string; 
  constructor(private http: HttpClient,public db:AngularFirestore,public router:Router) { }
  
  classify():Observable<any>{
    console.log(this.doc);
    let json = {
      "articles": [
        {
          "text": this.doc
        },
      ]
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        let final = res.body.replace('[','');
        final = final.replace(']','');
        console.log(final);
        return final;      
      })
    );      
    }
    addClassification(userID:string, doc:string,category:string)
  {
    console.log('userID in addClassification(): ', userID)
    console.log('doc in addClassification(): ', doc)
    console.log('category in addClassification(): ', category)
    //this.db.collection('Books').add(book);
    if(category=="selected")
    {
      category = "selected"
    }
    else
    {
      const article={ userID:userID, doc:doc, category:category}
    }
    const article={ userID:userID, doc:doc, category:category}
    this.userCollection.doc(userID).collection('Articles').add(article);
    console.log("article: " ,article)
    this.router.navigate(['/classified'])    
  }
  }
