import { User } from './interfaces/userauth';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;
  
  constructor(
    public afAuth:AngularFireAuth,
    private router:Router
    ) {
    this.user = this.afAuth.authState;
  }

  SignUp(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(res => {
          console.log('Succesful sign up',res);
          this.router.navigate(['/books']);
          // this.user = this.afAuth.authState;
        }
        ).catch(err => {
          alert(err.message);
        });

  }

  Logout(){
    this.afAuth.auth.signOut().then(res => {
      this.router.navigate(['/login']);
    }).catch(err => {
      alert(err.message);
    });
    
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(res => {
          console.log('Succesful Login',res);
          this.router.navigate(['/books']);
        }
        ).catch(err => {
          alert(err.message);
        })
  }



}